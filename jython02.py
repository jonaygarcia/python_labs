# jython foo.py -J-Xmx25m -J-Xms50m
'''
    Para ejecutarlo:
        # jython foo.py -J-Xms25m -J-Xmx50m
'''

class Hello:
   def __init__(self, name="John Doe"):
      self.name = name

   def greeting(self):
      print "Hello, %s" % self.name

jane = Hello("Jane Doe")
joe = Hello("Joe")
default = Hello()

jane.greeting()
joe.greeting()
default.greeting()
