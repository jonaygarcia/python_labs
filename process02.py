#!/usr/bin/env python
import os
import time

def child():
    print 'A new child ',  os.getpid( )
    time.sleep(2);
    os._exit(0)  

def parent(num_process):
    newpid = os.fork()
    if newpid == 0:
        #We are the child
        child()
    else:
        #We are the parent
        os.waitpid(newpid, 0) #Make sure process child cleaned up
        pids = (os.getpid(), newpid)
        print "parent: %d, child: %d" % pids

def main():
    for i in range(1,5):
        parent(i)

if __name__ == "__main__":
    main()
