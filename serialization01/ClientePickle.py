'''
Created on 10/01/2013

@author: jonaygarcia
'''

import os
try:
    import cPickle as pickle
except ImportError:
    import pickle


class ClientePickle():
    '''
    Implementacion de la clase ClientePickle
    '''
    def __init__(self, filename):
        '''
        Constructor con parametros
        '''
        self.__filename = filename
    
    
    def single_write(self, modo, cliente):
        '''
        Escribe un objeto cliente a una fichero.
        '''
        file = open(self.__filename, modo)
        pickle.dump(cliente, file)
        file.close()
        
        
    def multiple_write(self, modo, clientes):
        '''
        Escribe una lista de objetos cliente a un fichero.
        '''
        file = open(self.__filename, modo)
        for cliente in clientes:
            pickle.dump(cliente,file)
        file.close()
        
        
    def multiple_append(self, clientes):
        '''
        Anade una lista de objetos cliente a un fichero.
        '''
        file = open(self.__filename, 'ba')
        for cliente in clientes:
            pickle.dump(cliente,file)
        file.close()
        
    def multiple_read(self):
        '''
        Lee los objetos cliente de un fichero.
        '''
        clientes = []
        file = open(self.__filename, 'rb')
        while 1:
            try:
                clientes.append(pickle.load(file))
            except EOFError:
                break
            except pickle.PicklingError:
                break
        file.close()
        return clientes

    def escritura_simple_en_fichero(self, cliente):
        '''
        Si el fichero existe, anade al final del fichero, en caso contrario, 
        escribe el objeto al final del fichero.
        '''
        if os.path.exists(self.__filename):
            modo = 'ba'
        else:
            modo = 'wb'
        self.single_write(modo, cliente)
 
    def escritura_multiple_en_fichero(self, clientes):
        '''
        Si el fichero existe, anade al final del fichero, en caso contrario, 
        escribe el objeto al final del fichero.
        '''
        if os.path.exists(self.__filename):
            modo = 'ba'
        else:
            modo = 'wb'
        self.multiple_write(modo, clientes) 
            
    def buscar_en_fichero(self, dni):
        '''
        Busca en un fichero a ver si existe un cliente con dni determinado.
        '''
        file = open(self.__filename, 'rb')
        while 1:
            try:
                cliente = pickle.load(file)
                if cliente.get_dni() == dni:
                    return cliente
            except EOFError:
                break
            except pickle.PicklingError:
                break
        file.close()
        return None
        
    def eliminar_cliente(self, dni):
        '''
        Eliminar un cliente del fichero
        @param dni dni a borrar
        @return devuelve True si el cliente se encuentra en el fichero,
        False en caso contrario
        '''
        success = False
        clientes = self.multiple_read()
        for cliente in clientes:
            if cliente.get_dni() == dni:
                clientes.remove(cliente)
                success = True
        
        if success:
            self.multiple_write('wb', clientes)
        
        return success
