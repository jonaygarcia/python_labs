'''
Created on 10/01/2013

@author: jonaygarcia
'''

class Cliente:
    '''
    classdocs
    '''
        
    def __init__(self, nombre, dni):
        '''
        Constructor
        '''
        self.__nombre = nombre
        self.__dni = dni
        
    #def set_cliente(self, nombre, dni):
    #    self.__nombre = nombre
    #    self.__dni = dni
        
    def get_nombre(self):
        return self.__nombre
    
    def get_dni(self):   
        return self.__dni
    
    def imprimir(self):
        print "- Cliente: Nombre("+self.__nombre+"), DNI("+self.__dni+")"
        
    def validar_nif(self):   
        tabla="TRWAGMYFPDXBIJZSQVHLCKE"
        numeros="1234567890"
    
        success = False
        if (len(self.__dni)==9):
            letraControl=self.__dni[8].upper()
            dni=self.__dni[:8]
            if (len(dni)==len([n for n in dni if n in numeros])):
                if tabla[int(dni)%23]==letraControl:
                    success = True
        return success
    
