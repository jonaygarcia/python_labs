'''
Created on 10/01/2013

@author: jonaygarcia
'''

from Cliente import Cliente
from ClientePickle import ClientePickle
import os
try:
    import cPickle as pickle
except ImportError:
    import pickle

filename = "clientes.dat"
cp = ClientePickle(filename)

def Uno():
    nombre = raw_input("Introducir nombre: ")
    dni = raw_input("Introducir dni (clave principal): ")
    
    cliente = Cliente(nombre, dni)
    
    if (cliente.validar_nif()):
        if os.path.exists(filename):
            cliente_file = cp.buscar_en_fichero(dni)
            if cliente_file == None:
                cp.escritura_simple_en_fichero(cliente)
                print("Datos del cliente guardados.")
            else:
                print("El cliente con DNI " + dni + " ya existe " + filename + ".")
                cliente_file.imprimir()
        else:
            cp.escritura_simple_en_fichero(cliente)
    else:
        print("El dni " + cliente.get_dni() + "no es valido.")

 
 
def Dos():
    #cp = ClientePickle(filename)
    if os.path.exists(filename):
        clientes = cp.multiple_read()
    
        for cliente in clientes:
            cliente.imprimir()
 
def Tres():
    dni = raw_input("Introducir dni: ")
    
    cliente = cp.buscar_en_fichero(dni)
    
    if cliente == None:
        print("El cliente con DNI " + dni + " no se encuentra en el fichero " + filename + ".")
    else:
        print("Datos del cliente: ")
        cliente.imprimir()
     
def Cuatro():
    dni = input("Introducir dni:")
    
    success = cp.eliminar_cliente(dni)
    if (success):
        print("El cliente con DNI " + dni + " ha sido eliminado del fichero " + filename)
    else:
        print("El cliente con DNI " + dni + " no se encuentra en el fichero " + filename)
        
def Cinco():
    if os.remove(filename):
        print("Fichero " + filename + " borrado con exito.")
    else:
        print("El fichero " + filename + " no se ha podido borrar.")
        
def Seis():
    print ("Lista de DNIs validos:")
    print (" 1.- 11111111H")
    print (" 2.- 22222222J")
    print (" 3.- 33333333P")
    print (" 4.- 44444444A")
    print (" 5.- 55555555K")
    print (" 6.- 66666666Q")
    print (" 7.- 77777777B")
    print (" 8.- 88888888Y")
    print (" 9.- 99999999R")
    print ("10.- 12345678Z")
 
def menu():
    while True:
        switch = { 
            1:Uno,
            2:Dos,
            3:Tres,
            4:Cuatro,
            5:Cinco,
            6:Seis,
            }
        print("MENU:")
        print("1.- Introducir un nuevo cliente.")
        print("2.- Listar clientes.")
        print("3.- Buscar cliente.")
        print("4.- Eliminar cliente.")
        print("5.- Eliminar fichero (" + filename + ").")
        print("6.- Listar dni validos.")
        print("0.- Salir")
        opcion = int(input("> "))
        if opcion == 0:
            break
        else:
            switch[opcion]()
 

if __name__ == '__main__':
    menu()

