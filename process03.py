#!/usr/bin/env python
import os
import sys
import time

def parent():
    r, w = os.pipe() #Create two ends - one for reading, and one for writing.

    newpid = os.fork()
    
    if newpid == 0: 
        #We are the child
        print 'A new child ',  os.getpid( )

        os.close(r)
        w = os.fdopen(w, 'w')
        print "child: writing..."
        w.write("Soy tu hijo")
        w.close()

        time.sleep(2)
        os._exit(0)  


    else: 
        #We are the father
        os.close(w)      #Close file descriptor
        r = os.fdopen(r) #Turn r into a file object
        txt = r.read()   #Read for the file
        os.waitpid(newpid,0) #Make sure the child process gets cleaned up
        
        pids = (os.getpid(), newpid)
        print 'child said: "'+txt+'"'
        print "parent: %d, child: %d" % pids

def main():
    parent()

if __name__ == "__main__":
    main()
